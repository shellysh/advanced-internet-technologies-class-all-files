//import { environment } from './../environments/environment.prod';
import { environment } from './../environments/environment';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { MessagesService } from './messages/messages.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesFormsComponent } from './messages/messages-forms/messages-forms.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersComponent } from './users/users.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { LoginComponent } from './login/login.component';

//התקנת פיירבייס
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { MessagesfComponent } from './messagesf/messagesf.component';
//import { environment } from './../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    MessagesFormsComponent,
    NavigationComponent,
    UsersComponent,
    NotFoundComponent,
    MessageComponent,
    LoginComponent,
    MessagesfComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path: '', component: MessagesComponent},
      {path: 'users', component: UsersComponent},
      {path: 'message/:id', component: MessageComponent},
      {path: 'login', component: LoginComponent},
      {path: 'messagesf', component: MessagesfComponent},
      {path: '**', component: NotFoundComponent}
    ])
  ],
  providers: [
    MessagesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
