import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesFormsComponent } from './messages-forms.component';

describe('MessagesFormsComponent', () => {
  let component: MessagesFormsComponent;
  let fixture: ComponentFixture<MessagesFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
