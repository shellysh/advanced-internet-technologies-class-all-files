import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
//פיירבייס
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/Rx';

@Injectable()
export class MessagesService {

  //שמאל תכונה, ימין סוג המחלקה
  http:Http; //המחלקה מנהלת את התקשורת עם השרת, ספציפי עם הרסט איפיאי

  getMessages(){
   //get messages from the SLIM rest API (No DB)
   //טוקן של כל בקשה להתחברות, אישור לקבלת נתונים
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
   return this.http.get(environment.url + 'messages',options);
  }

  //השרת שמתחבר לפיירבייס
  getMessagesFire(){
    //הוויליו מייצר את האובזווריבל
    return this.db.list('/messages').valueChanges();
  }

  postMessage(data){
    let token = localStorage.getItem('token');
    //המרת גייסון למפתח וערך
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded',
        'Authorization':'Bearer '+token
      })
    }

    let params = new HttpParams().append('message',data.message);
    
    return this.http.post(environment.url + 'messages', params.toString(), options);

  }

  deleteMessage(key){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
    return this.http.delete(environment.url + 'messages/'+ key, options);
  }

  //הודעה בודדת
  getMessage(id){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
     return this.http.get(environment.url + 'messages/'+ id, options);
  }
  
  login(credentials){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }

    let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
    //מאפ פועל על המידע שהגיע מהשרת JWT
    return this.http.post(environment.url + 'auth', params.toString(), options).map(response=>{
        let token = response.json().token; //JWT שהגיע מהשרת
        if(token) localStorage.setItem('token' , token);
    });
  }

  //בתוך הקונסטרקטור נוצר מופע של האובייקט, דפנדיסי אינג'קטיון
  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }

}
