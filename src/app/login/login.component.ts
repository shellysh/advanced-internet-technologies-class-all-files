import { MessagesService } from './../messages/messages.service';
import { Component, OnInit} from '@angular/core';
import {FormGroup , FormControl} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  invalid = false;
  //Reactive Form
  loginform = new FormGroup({
    user:new FormControl(),
    password:new FormControl()
  });

  login(){
    this.service.login(this.loginform.value).subscribe(response=>{
      this.router.navigate(['/']); //אם הכל תקין נעבור לדף הודעות
    } , error=>{
      this.invalid = true;
    })
  }

  logout(){
    localStorage.removeItem('token');
    this.invalid = false;
  }

  constructor(private service:MessagesService, private router:Router ) { }

  ngOnInit() {
  }

}
